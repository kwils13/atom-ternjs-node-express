atom-ternjs / node-express plugin
---
### Installation

1. `cd` to `~/.atom/packages/atom-ternjs/node_modules/tern/plugin`

2. `git init`

3. `git remote add origin https://<user>@bitbucket.org/kwils13/atom-ternjs-node-express`

4. `git pull origin master`

5. update project's `.tern-project` file to reference `node-express` plugin
```
    "node-express": {},
```
